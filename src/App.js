import React, { Component } from 'react'
import { Switch, Route} from "react-router-dom"
import User from "./containers/user/index"
import Journal from './containers/journal/index'
class App extends Component {
  render() {
    return (
      <div className="App">

      <Switch>
        < Route exact path ="/" component = {User} />
        < Route path='/dashboard' component={Journal} />
      </Switch>
      </div>
    );
  }
}

export default App;
