import request from 'request-promise'
import { openNotificationWithIcon as notify } from  '../../../common/nortification'

export const sPost = async () => {
    let val = false
    let options = {
        withCredentials : true,
        method: 'GET',
        uri: 'https://protected-brushlands-25174.herokuapp.com/cookies/check',
        headers: {
            'content-type': 'application/x-www-form-urlencoded',
            'Origin': 'http://localhost:3000/',
            'Access-Control-Request-Method': 'GET',
        }
    }

    await request(options).then( data => {
        if (data) {
            val = JSON.parse(data)
        }
    }).catch(e => {
        notify( 'error', 'Cookie Ping crashed', 'Sorry, you\'ll need to login again')
        // nortify( 'error', 'Signin failed', 'Internal server error, bear with us')
    })
    return val
}