import React, { Component } from 'react';
import { Row, Col } from "antd";
import { connect } from 'react-redux'
import { Redirect } from "react-router-dom";
import Header from "../../common/header";
import Datepicker from './datePicker/datepicker'
import TextInput from './TextInput/textInput'
import Card1 from './cards/card1'
import { sPost } from './cookies/cookiesPing'
import { openNotificationWithIcon as notify } from  '../../common/nortification'
class Journal extends Component {

    constructor () {
        super ()
        this.state = {
            redirect : false,
            user : {}
        }
    }



    async componentDidMount () {
        
        await sPost().then(data => {
            console.log(` name ${data}`)    
            this.setState({user : data})
        }).catch(e => console.log(e))

        if (this.props.user===undefined && this.state.user===false){
            notify( 'error', 'Session expired', 'Sorry, you\'ll need to login again')
            this.setState({redirect : true})
        }
        console.log(` name ${this.state.user.fname}`)

    
    
}
    
    render() {
        if(this.state.redirect){
            console.log(`Inside render`)
            return(<Redirect to='/' />)
        }
        return (
            
            <div className="gutter-example">
                <Header />
                <div style={{margin : 10}}>
                {this.props.user.fname}
                <Row gutter={32}>
                    < Col className="gutter-row" gutter={32} xs={24} sm={24} md={6} lg={6} xl={6}>
                        <div className="gutter-box">
                        <Datepicker />
                        </div>
                    </ Col>
                    < Col className="gutter-row" gutter={32} xs={24} sm={24} md={18} lg={18} xl={18}>
                    
                        < Col className="gutter-row" gutter={32} xs={24} sm={24} md={24} lg={24} xl={24}>
                            <Card1 />
                        </ Col>
                        < Col className="gutter-row" gutter={32} xs={24} sm={24} md={24} lg={24} xl={24}>
                            <TextInput />
                        </ Col>

                    </ Col>
                </Row>
                </div>
            </div>
        );
    }

}

const mapStateToProps = state => {
    let user
    if (state.USER.user.fname !== undefined ){
        user = state.USER.user
    } else if (this.state !== undefined) {
        user = this.state.user
    }
    if(user!==undefined){
        console.log(`user in map ${user.fname}`)

    }
    return {
        user
    };
}


const mapDispatchToProps = dispatch => {
    dataSet : async () => {
        sPost().then(data => {
            dispatch()
        }).catch(e => console.log(e))
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(Journal);
