import React, { Component } from 'react';
import { Input } from 'antd';
import './../../../styles/common.styles.css'
const { TextArea } = Input;


export default class TextInput extends Component {
    render () {
        return (
            <div className='container'>
                <TextArea placeholder="Tell me about your day" autosize={{ minRows: 12}} />
            </div>
        )
    }
}
