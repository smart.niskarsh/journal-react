import request from "request-promise";
import { login } from '../../../redux/actions/userActions'
import store from '../../../redux/store'
import { openNotificationWithIcon as notify } from  '../../../common/nortification'

export const sPost = async (values) => {
    let val = false
    let options = {
        withCredentials : true,
        method: 'POST',
        uri: 'https://protected-brushlands-25174.herokuapp.com/signin',
        headers: {
            'content-type': 'application/x-www-form-urlencoded',
            'Origin': 'http://localhost:3000/',
            'Access-Control-Request-Method': 'POST',
        },
        form: {
            email: values.emailLogin,
            password: values.passwordLogin,            
        }

    }

    await request(options).then( data => {
        store.dispatch( login(JSON.parse(data)) );
        notify( 'success', `Hi ${JSON.parse(data).nickname}`, `You\'re in`)
        val = true
    }, () => {
        notify( 'error', 'Signin failed', 'Invalid creds, try again')
    }).catch((e) => {
        notify( 'error', 'Signin failed', 'Invalid creds, try again')
        // notify( 'error', 'Signin failed', 'Internal server error, bear with us')
    });

    return val
}