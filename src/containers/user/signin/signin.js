import React, { Component } from "react"
import { Form, Icon, Input, Button } from 'antd';
import { Redirect } from "react-router-dom"

import { sPost } from './signinPost'
import '../../../styles/forms.common.styles.css'
const FormItem = Form.Item

class Signin extends Component {
  constructor() {
    super()
    this.state = {
      redirectStatus : false,
      redirectURI : ''
    }
  }
  
  
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        let a
        await sPost(values).then(data => {
          if (data) {
            this.setState(function (state, props) {
              return  {...state, redirectStatus : true, redirectURI : '/dashboard' }
            })
          }
        }).catch(e => console.log(e));
      }
    });
  }
  render() {

    if(this.state.redirectStatus){
      return (<Redirect to={this.state.redirectURI} />)
    }
    const { getFieldDecorator } = this.props.form;
    return (
      
      <Form onSubmit={this.handleSubmit}  className="login-form">
        <FormItem className='item'>
          {getFieldDecorator('emailLogin', {
            rules: [{
              type: 'email', message: 'The input is not valid E-mail!',
            }, {
              required: true, message: 'Please input your E-mail!',
            }],
          })(
            <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} name="email" placeholder="myEmailId@something.haa" />
          )}
        </FormItem>
        <FormItem className='item'>
          {getFieldDecorator('passwordLogin', {
            rules: [{ required: true, message: 'Please input your Password!' }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" name="password" />
          )}
        </FormItem>
        <FormItem className='item but'>

          <Button type="primary" htmlType="submit" className="login-form-button">
            Log in
              </Button>

        </FormItem>
      </Form>
    );
  }
}




const Ws = Form.create()(Signin);
export default Ws;