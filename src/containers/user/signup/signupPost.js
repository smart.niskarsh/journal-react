import request from "request-promise";
import { openNotificationWithIcon as notify } from  './../../../common/nortification'
export const sPost = values => {
    let options = {
        withCredentials : true,
        method: 'POST',
        uri: 'https://protected-brushlands-25174.herokuapp.com/signup',
        headers: {
            'content-type': 'application/x-www-form-urlencoded',
            'Origin': 'http://localhost:3000/',
            'Access-Control-Request-Method': 'POST'
        },
        form: {
            fname: values.fname,
            lname: values.lname,
            email: values.email,
            password: values.password,
            nickname: values.nickname,
            phone: values.phone
        }

    }
    request(options).then(() => {
        notify( 'success', 'Signup successful', 'Try logging in now')
    }, () => {
        notify( 'error', 'Signup failed', 'Try signing up again')
    }).catch((e) => {
        notify( 'error', 'Signup failed', 'Try signing up again')
    });
}

