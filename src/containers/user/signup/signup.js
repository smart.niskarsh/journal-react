import React, { Component } from "react";
import { Form, Input, Tooltip, Icon, Select, Button } from 'antd';
import { sPost } from './signupPost';
import '../../../styles/forms.common.styles.css'
const FormItem = Form.Item;
const Option = Select.Option;


class Signup extends Component {
  constructor () {
    super()
    this.state = {
      confirmDirty: false,
      autoCompleteResult: []
    }
  }
  
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        await sPost(values)
      }
    });
  }
  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }


  checkNames = (rule, value, callback) => {

    let fname = value;
    while (fname) {
      try {
        let a = fname.charAt(0);
        if (a === " ") {
          fname = fname.substring(1);
          callback("I'm afraid this is not you first name");
        } else if ((parseInt(a, 10) + 48) >= 48 && (parseInt(a, 10) + 48) <= 57) {
          console.log(`In here`);
          fname = fname.substring(1);
          callback("I'm afraid this is not you first name");
        } else {
          fname = fname.substring(1);
          console.log(fname);
        }
      } catch (e) {
        fname = false;
      }


    }
    callback();

  }

  checkNumber = (rule, value, callback) => {

    let number = value || "";
    if (number.length > 10) {
      callback("I'm afraid this is not a valid phone number!!!");
    }
    while (number) {
      try {
        let a = number.charAt(0);

        if (!((parseInt(a, 10) + 48) >= 48 && (parseInt(a, 10) + 48) <= 57)) {
          number = number.substring(1);
          callback("I'm afraid this is not a valid phone number!!!");
        } else {
          number = number.substring(1);
          console.log(number);
        }
      } catch (e) {
        number = false;
      }


    }
    callback();

  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  }
  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }
  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      
      wrapperCol: {
        xs: { 
          span: 24,
        },
        sm: { span: 24 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
        },
        sm: {
          span: 24,
        },
      },
    };
    const prefixSelector = getFieldDecorator('prefix', {
      initialValue: '91',
    })(
      <Select style={{ width: 70 }}>
        <Option value="91">+91</Option>
        <Option value="87">+87</Option>
      </Select>
    );



    return (

      
      

      <Form onSubmit={this.handleSubmit}>
        <FormItem {...formItemLayout}  className='item' >
          {
            getFieldDecorator('fname', {
              rules: [{
                required: true, message: "It's odd you don't wanna share, but I need it anyway!!! "
              }, {
                validator: this.checkNames
              }]
            })(<Input name="fname" placeholder="my first name" />)
          }

        </FormItem>

        <FormItem {...formItemLayout}  className='item'>
          {
            getFieldDecorator('lname', {
              rules: [{
                required: true, message: "It's odd you don't wanna share, but I need it anyway!!! "
              }, {
                validator: this.checkNames
              }]
            })(<Input name="lname" placeholder="my last name" />)
          }

        </FormItem>


        <FormItem
          {...formItemLayout}
          className='item'
        >
          {getFieldDecorator('email', {
            rules: [{
              type: 'email', message: 'The input is not valid E-mail!',
            }, {
              required: true, message: 'Please input your E-mail!',
            }],
          })(
            <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} name="email" placeholder="myEmailId@something.haa" />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}  className='item'
          >
          {getFieldDecorator('password', {
            rules: [{
              required: true, message: 'Please input your password!',
            }, {
              validator: this.validateToNextPassword,
            }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" name="password" placeholder="******" />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}  className='item'
        >
          {getFieldDecorator('confirm', {
            rules: [{
              required: true, message: 'Please confirm your password!',
            }, {
              validator: this.compareToFirstPassword,
            }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" onBlur={this.handleConfirmBlur} name="cpassword" placeholder="******" />
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}  className='item'
        >
          {getFieldDecorator('nickname', {
            rules: [{ required: true, message: 'Please input your nickname!', whitespace: true }],
          })(
            <Input name="nickname" placeholder="my nickname" />
          )}
        </FormItem>
        <FormItem  className='item'
          {...formItemLayout}
        >
          {getFieldDecorator('phone', {
            rules: [
              {
                required: true, message: 'Please input your phone number!'
              }, {
                validator: this.checkNumber
              }],
          })(
            <Input addonBefore={prefixSelector} style={{ width: '100%' }} name="pno" placeholder="1234567890" />
          )}
        </FormItem>

        <FormItem {...tailFormItemLayout}  className='item but'>
          <Button type="primary" htmlType="submit">Sign me up</Button>
        </FormItem>
      </Form>
    );
  }
}

const fSignup = Form.create()(Signup);
export default fSignup;

























