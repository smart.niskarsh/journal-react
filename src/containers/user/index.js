import React, { Component } from 'react';
import { Row, Col } from "antd";
import Signin from "./signin/signin";
import Signup from "./signup/signup";
import Header from "../../common/header";
import './../../styles/home.styles.css'



class User extends Component {
    
    render() {
        return(
            <div className="gutter-example">
                <Header />
                <div className='home_div'>
                <Row gutter={24}>
                < Col className="gutter-row" gutter={24} xs={24} sm={24} md={8} lg={8} xl={8}>
                
                </ Col>                
                < Col className="gutter-row"  gutter={24} xs={24} sm={24} md={8} lg={8} xl={8}>
                    <div className="gutter-box"><Signin /></div>
                </ Col>
                < Col className="gutter-row" gutter={24} xs={24} sm={24} md={8} lg={8} xl={8}>
                    <div className="gutter-box"><Signup />   </div>
                </ Col>                
                </Row>
                </div>
            </div>
        );
    }
}

export default User;