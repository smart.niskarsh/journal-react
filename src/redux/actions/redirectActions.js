export const REDIRECT_TRUE = 'REDIRECT_TRUE' 


export const redirect = uri => {
    return {
        type : REDIRECT_TRUE,
        uri
    }
}