import { userReducer as USER } from './userSigninReducer'
import { redirectReducer as REDIRECT } from './redirectReducer'
import { combineReducers } from 'redux'
const reducer = combineReducers({
    USER
    // REDIRECT
});
export default reducer;