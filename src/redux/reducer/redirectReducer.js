import { REDIRECT_TRUE } from './../actions/redirectActions'
import store from './../store'

let initialState = {
    // ...state, 
    redirectStatus : false,
    redirectURI : ''
}


export const redirectReducer = ( state=initialState, action ) => {
    switch (action.type) {
        case REDIRECT_TRUE : {
            return {...state, redirectStatus : true, redirectURI : action.uri}  
        }
        default : {
            return state;
        }     
    }
}

