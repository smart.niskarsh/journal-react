import { LOGIN_USER, LOGOUT_USER } from './../actions/userActions'


let initialState = {
    LogStatus : LOGOUT_USER,
    user : {}
}


export const userReducer = ( state=initialState, action ) => {
    switch (action.type) {
        case LOGIN_USER : {
            return {...state, user : action.user, LogStatus : LOGIN_USER}  
        }
        case LOGOUT_USER : {
            return {...state, user : {}, LogStatus : LOGOUT_USER} 
        }
        default : {
            return state;
        }     
    }
}

