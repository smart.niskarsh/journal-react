import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger'
import reducer from './reducer/combineReducer';

const middleware = applyMiddleware(logger);

let store = createStore( reducer, middleware );
store.subscribe(() => {
    console.log(store.getState());
});

export default store;