import React, { Component } from 'react';
import { Layout } from 'antd';
import "../styles/header.styles.css";
const { Header } = Layout;


class Head extends Component {
    render() {
        return (
            <Layout>
                <Header className="header">Journals</Header>
            </Layout>
        );
    }
}

export default Head;